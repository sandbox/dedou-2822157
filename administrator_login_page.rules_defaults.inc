<?php
/**
 * @file
 * administrator_login_page.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function administrator_login_page_default_rules_configuration() {
  $items = array();
  $items['rules_administrator_redirect_after_logging_in'] = entity_import('rules_config', '{ "rules_administrator_redirect_after_logging_in" : {
      "LABEL" : "Administrator redirect after logging in",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_login" : [] },
      "DO" : [ { "redirect" : { "url" : "admin" } } ]
    }
  }');
  $items['rules_visitors_administrator_page_redirect_to_admin_login'] = entity_import('rules_config', '{ "rules_visitors_administrator_page_redirect_to_admin_login" : {
      "LABEL" : "Visitors Administrator page redirect to  admin login page",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "init" : [] },
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "AND" : [ { "path_is_admin_page" : { "path" : "[site:current-page:path]" } } ] }
      ],
      "DO" : [ { "redirect" : { "url" : "admin-login" } } ]
    }
  }');
  return $items;
}
